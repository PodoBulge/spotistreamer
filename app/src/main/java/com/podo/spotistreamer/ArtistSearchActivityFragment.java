package com.podo.spotistreamer;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.podo.spotistreamer.model.Artist;
import com.podo.spotistreamer.model.ArtistLab;

import java.util.List;

public class ArtistSearchActivityFragment extends Fragment {

    private List<Artist> mArtists;
    private ListView listView;

    public ArtistSearchActivityFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_artist_search, container, false);
        listView = (ListView)rootView.findViewById(R.id.artist_search_listView);
        mArtists = ArtistLab.getInstance(getActivity()).getArtists();
        ArtistAdapter artistAdapter = new ArtistAdapter(getActivity(), mArtists);
        listView.setAdapter(artistAdapter);

        return rootView;
    }
}
