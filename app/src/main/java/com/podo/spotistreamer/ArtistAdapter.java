package com.podo.spotistreamer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.podo.spotistreamer.model.Artist;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 *
 */
public class ArtistAdapter extends BaseAdapter {

    private Context mContext;
    private List<Artist> mArtists;

    public ArtistAdapter(Context mContext, List<Artist> mArtists) {
        this.mContext = mContext;
        this.mArtists = mArtists;
    }

    static class ViewHolder{
        TextView artistName;
        ImageView albumCover;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if(convertView == null){
            LayoutInflater inflater =
                        (LayoutInflater)mContext
                                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.individual_search_artist_result, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.artistName = (TextView)convertView.findViewById(R.id.textView_artist);
            viewHolder.albumCover = (ImageView)convertView.findViewById(R.id.imageView_artist_serch_result_albumCover);
            convertView.setTag(viewHolder);
        } else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        Artist artist = (Artist)getItem(position);
        viewHolder.artistName.setText(artist.getName());
        Picasso.with(mContext)
                .load(artist.getArtistThumbnailImageUrl())
                .resizeDimen(R.dimen.individual_search_album_cover_size, R.dimen.individual_search_album_cover_size)
                .into(viewHolder.albumCover);

        return convertView;
    }

    @Override
    public int getCount() {
        return mArtists.size();
    }

    @Override
    public Object getItem(int position) {
        return mArtists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
