package com.podo.spotistreamer.model;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by podo on 02.06.15.
 */
public class ArtistLab {
    private List<Artist> mArtists;
    private static ArtistLab sArtistLab;
    private Context mAppContext;

    private ArtistLab(Context context){
        mAppContext = context;
        mArtists = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            Artist artist = new Artist("Артист"+i, "id", "https://i.scdn.co/image/79e91d3cd4a7c15e0c219f4e6c941d282fe87a3d");
            mArtists.add(artist);

        }
    }

    public static ArtistLab getInstance(Context context){
        if(sArtistLab == null){
            sArtistLab = new ArtistLab(context);
        }
        return sArtistLab;
    }

    public List<Artist> getArtists() {
        return mArtists;
    }

    public Artist getArtist(UUID id){
        for(Artist a: mArtists){
            if(a.getId().equals(id)){
                return a;
            }
        }
        return null;
    }
}
