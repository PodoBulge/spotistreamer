package com.podo.spotistreamer.model;

import java.util.UUID;

/**
 * Created by podo on 02.06.15.
 */
public class Artist {
    private UUID id;
    private String name;
    private String spotifyID;
    private String artistThumbnailImageUrl;

    public Artist(String name, String spotifyID, String artistThumbnailImageUrl) {
        this.name = name;
        this.spotifyID = spotifyID;
        this.artistThumbnailImageUrl = artistThumbnailImageUrl;
        this.id = UUID.randomUUID();
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpotifyID() {
        return spotifyID;
    }

    public void setSpotifyID(String spotifyID) {
        this.spotifyID = spotifyID;
    }

    public String getArtistThumbnailImageUrl() {
        return artistThumbnailImageUrl;
    }

    public void setArtistThumbnailImageUrl(String artistThumbnailImageUrl) {
        this.artistThumbnailImageUrl = artistThumbnailImageUrl;
    }


}
